#!/usr/bin/env python3

# File:
#  	shopping-list.py
#
# Author:
#   James Dooley
#
# Created:
#   14.12.19
#
# Usage:
#   python3 shopping-list.py
#
# Python Version:
#   3.7.0
#

"""shopping-list.py: """

__author__ = "James Dooley"
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "James Dooley"
__email__ = ""
__status__ = "Prototype"

num2words = {1: 'first', 2: 'second', 3: 'third', 4: 'fourth', 5: 'fifth', \
             6: 'sixth', 7: 'seventh', 8: 'eighth', 9: 'ninth', 10: 'tenth', \
            11: 'eleventh', 12: 'twelfth', 13: 'thirteenth', 14: 'fourteenth', \
            15: 'fifteenth', 16: 'sixteenth', 17: 'seventeenth', 18: 'eighteenth', \
            19: 'nineteenth', 20: 'twentieth'}

shoppingList = []
wordIndex = 1

print("\t*** Shopping List ***\n")

while True:
    item = input("\tEnter the " + num2words[wordIndex] + " item: ")

    if not item:
        break

    shoppingList.append(item)
    wordIndex = wordIndex + 1

    if wordIndex > 20:
        print("\nThis shopping list only supports 20 items..")
        break

print("\n\tThanks for using our program.  You entered the following items:")

for item in shoppingList:
    print("\t\t" + item)

print("\n\tYou entered a total of " + str(len(shoppingList)) + " items.")